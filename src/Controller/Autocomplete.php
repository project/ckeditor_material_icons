<?php

namespace Drupal\ckeditor_material_icons\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Render\FormattableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class Autocomplete extends ControllerBase {

  const META_URL = 'https://fonts.google.com/metadata/icons';

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Autocomplete constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \GuzzleHttp\Client $client
   *   The http client.
   */
  public function __construct(CacheBackendInterface $cache, Client $client) {
    $this->cache = $cache;
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.data'),
      $container->get('http_client')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = mb_strtolower(array_pop($typed_string));

      // Load the icon data so we can check for a valid icon.
      $iconData = $this->getIconMeta();

      // Check each icon to see if it starts with the typed string.
      if (!empty($iconData['icons'])) {
        foreach ($iconData['icons'] as $data) {
          $icon = $data['name'];

          // Starts with.
          if (strpos($icon, $typed_string) === 0) {
            $results[$icon] = [
              'value' => $icon,
              'label' => new FormattableMarkup('<div class="mi-result"><i class="material-icons">:icon</i><span>:icon</span></div>', [':icon' => $icon]),
              'weight' => 1,
            ];
          }
          // If the string is found.
          elseif (strpos($icon, $typed_string) === 0) {
            $results[$icon] = [
              'value' => $icon,
              'label' => new FormattableMarkup('<div class="mi-result"><i class="material-icons">:icon</i><span>:icon</span></div>', [':icon' => $icon]),
              'weight' => 2,
            ];
          }
        }

        // Add in tag matches as a helper.
        if (count($results) < 10) {
          foreach ($iconData['icons'] as $data) {
            $icon = $data['name'];
            $tags = !empty($data['tags']) ? $data['tags'] : [];

            foreach ($tags as $tag) {
              if (strpos($tag, $typed_string) === 0) {
                $results[$icon] = [
                  'value' => $icon,
                  'label' => new FormattableMarkup('<div class="mi-result"><i class="material-icons">:icon</i><span>:icon</span></div>', [':icon' => $icon]),
                  'weight' => 3,
                ];
              }
            }
          }
        }

        // Sort by weight.
        usort($results, function ($a, $b) {
          if ($a['weight'] === $b['weight']) {
            return 0;
          }
          return $a['weight'] < $b['weight'] ? -1 : 1;
        });
      }
    }
    return new JsonResponse(array_slice($results, 0, 10));
  }

  /**
   * Grabs icons from google.
   *
   * @todo create a fallback.
   *
   * @return array
   *   The icon list.
   */
  protected function getIconMeta() {
    // Check for cached icons.
    if (!$icons = $this->cache->get('materialicons.iconlist')) {
      // Parse the metadata file and use it to generate the icon list.
      $icons = [];
      // @todo fix handling exeptions here.
      $response = $this->client->get(self::META_URL);
      $status_code = $response->getStatusCode();
      if ($status_code === 200) {
        $body = $response->getBody()->getContents();

        // @todo Something is weird with the json response.
        $first_bracket = strpos($body, '{');
        if ($first_bracket !== 0) {
          $body = substr($body, $first_bracket);
        }

        if ($icons = JSON::decode($body)) {
          $this->cache->set('materialicons.iconlist', $icons, strtotime('+1 week'), [
            'materialicons',
            'iconlist',
          ]);
        }
      }
    }
    else {
      $icons = $icons->data;
    }

    return (array) $icons;
  }

}
