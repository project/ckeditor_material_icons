<?php

namespace Drupal\ckeditor_material_icons\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "material_icons" plugin.
 *
 * @CKEditorPlugin(
 *   id = "material_icons",
 *   label = @Translation("Material Icons")
 * )
 */
class MaterialIcons extends CKEditorPluginBase implements CKEditorPluginCssInterface {

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = drupal_get_path('module', 'ckeditor_material_icons') . '/js/plugins/material_icons';
    return [
      'material_icons' => [
        'label' => 'Material Icons',
        'image' => $path . '/icons/material_icons.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_material_icons') . '/js/plugins/material_icons/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor) {
    return ['https://fonts.googleapis.com/icon?family=Material+Icons'];
  }

}
