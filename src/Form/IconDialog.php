<?php

namespace Drupal\ckeditor_material_icons\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\editor\Ajax\EditorDialogSave;

/**
 * Creates an icon dialog form for use in CKEditor.
 *
 * @package Drupal\ckeditor_material_icons\Form
 */
class IconDialog extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckeditor_material_icons_dialog';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#attached']['library'][] = 'ckeditor_material_icons/dialog';

    $form['notice'] = [
      '#markup' => $this->t('<strong>Note:</strong> Only the default icon set is currenlty supported. Outline, Rounded, etc support not yet implemented.'),
    ];

    $form['icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon Name'),
      '#default_value' => '',
      '#required' => TRUE,
      '#description' => $this->t('Name of the Material Design Icon. See @iconsLink for valid icon names, or begin typing for an autocomplete list.', [
        '@iconsLink' => Link::fromTextAndUrl(
          $this->t('the icon list'),
          Url::fromUri('https://material.io/resources/icons', ['attributes' => ['target' => '_blank']])
        )->toString(),
      ]),
      '#autocomplete_route_name' => 'ckeditor_material_icons.autocomplete',
    ];

    $form['classes'] = [
      '#title' => $this->t('Additional Classes'),
      '#type' => 'textfield',
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Insert Icon'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $settings['icon'] = $form_state->getValue('icon');
    $settings['classes'] = $form_state->getValue('classes', '');

    $response->addCommand(new EditorDialogSave($settings));
    $response->addCommand(new CloseModalDialogCommand());

    return $response;
  }

}
